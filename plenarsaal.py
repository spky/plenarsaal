#!/usr/bin/env python3

from argparse import ArgumentParser
from collections import namedtuple
from datetime import datetime, timedelta
from email import charset
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate
from logging import (
    DEBUG, ERROR, INFO, WARNING, Formatter, StreamHandler, getLogger
)
from logging.handlers import RotatingFileHandler
from os import makedirs, path
from pprint import pformat
from random import choice
from smtplib import SMTP_SSL, SMTPException

from requests import get

LOG_LEVELS = dict(debug=DEBUG, error=ERROR, info=INFO, warning=WARNING)


def arguments():
    parser = ArgumentParser(__file__)
    parser.add_argument(
        '-d', '--dry', help='dry run (send mo mail)', action='store_true'
    )
    parser.add_argument(
        '-l', '--log', help='log file', default=path.abspath(
            path.join(path.dirname(__file__), 'log', 'file.log')
        )
    )
    parser.add_argument(
        '-v', '--lvl', help='log level', default='info',
        choices=(LOG_LEVELS.keys())
    )
    parser.add_argument(
        '--botname', help='bot name', default='Plenarsaal Bot'
    )
    parser.add_argument(
        '--apibase', help='api base url', default='https://wiki.cccmz.de'
    )
    parser.add_argument(
        '--apiuser', help='api user auth', required=True
    )
    parser.add_argument(
        '--apipass', help='api pass auth', required=True
    )
    parser.add_argument(
        '--pagespace', help='pages space', default='VB'
    )
    parser.add_argument(
        '--pagetitle', help='pages title', default='Plenum'
    )
    parser.add_argument(
        '--mailserver', help='smtp server', default='mail.cccmz.de'
    )
    parser.add_argument(
        '--mailport', help='smtp port', type=int, default=465
    )
    parser.add_argument(
        '--mailuser', help='smtp user', required=True
    )
    parser.add_argument(
        '--mailpass', help='smtp pass', required=True
    )
    parser.add_argument(
        '--mailto', help='send mail to', required=True
    )
    return parser.parse_args()


def logger(args, name):
    log = getLogger()
    formatter = Formatter('''
%(levelname)s - %(asctime)s | %(name)s
%(module)s.%(funcName)s [%(pathname)s:%(lineno)d]
    %(message)s
'''.lstrip())
    level = LOG_LEVELS.get(args.lvl, INFO)
    log.setLevel(DEBUG)

    stream = StreamHandler(stream=None)
    stream.setFormatter(formatter)
    stream.setLevel(level)
    log.addHandler(stream)

    parent = path.abspath(path.dirname(args.log))
    if not (path.exists(parent) and path.isdir(parent)):
        makedirs(parent)

    bucket = RotatingFileHandler(
        args.log, maxBytes=(1024 * 1024) * 2, backupCount=9
    )
    bucket.setFormatter(formatter)
    bucket.setLevel(DEBUG)
    log.addHandler(bucket)
    return getLogger(name)


class Plenarsaal(object):
    def __init__(self):
        self.args = arguments()
        self.log = logger(self.args, __name__)
        self.log.debug('+' * 32)

    def _url(self, *uri):
        return '/'.join([
            self.args.apibase.rstrip('/'), '/'.join(uri).lstrip('/')
        ])

    def request(self, *uri, **params):
        url = self._url(*uri)
        self.log.info('request "%s" "%s"', url, pformat(params))
        req = get(
            url, auth=(self.args.apiuser, self.args.apipass), params=params
        )
        if req.status_code == 200:
            res = req.json()
            self.log.debug('request ok, got "%s"', pformat(res))
            return res
        self.log.warning(
            'request not ok, got "%s" "%s"', req.status_code, pformat(req.text)
        )

    def query(self, dts):
        title = '{:04}-{:02}-{:02}'.format(dts.year, dts.month, dts.day)
        search = self.request('rest/api/search', cql='''
type = page and space.key = {pagespace} and title = "{title}"
        '''.strip().format(
            pagespace=self.args.pagespace,
            title='{} {}'.format(title, self.args.pagetitle)
        ))
        if not (search and search.get('results')):
            self.log.info('no search results for "%s"', title)
            return
        page_id = search['results'][0].get('content', {}).get('id')
        if not page_id:
            self.log.info('no page id in search results for "%s"', title)
            return
        page = self.request(
            'rest/api/content', page_id, expand='body.styled_view'
        )
        if not (page and page.get('body')):
            self.log.warning('no page body for "%s"', title)
            return
        body = page['body'].get('styled_view', {}).get('value')
        link = page.get('_links', {}).get('tinyui')
        if not (body and link):
            self.log.info('no page view for "%s"', title)
            return

        Post = namedtuple('post', ['title', 'body', 'link'])
        return Post(title, body, self._url(link))

    def mail(self, subject, message, attach=None):
        charset.add_charset('utf-8', charset.QP, charset.QP, 'UTF-8')
        res = MIMEMultipart()
        res.add_header('To', self.args.mailto)
        res.add_header('From', '{name} <{mailuser}>'.format(
            name=self.args.botname, mailuser=self.args.mailuser
        ))
        res.add_header('Subject', subject)
        res.add_header('Date', formatdate())
        res.add_header('X-Mailer', self.args.botname)
        res.attach(MIMEText(message, 'plain', 'UTF-8'))
        if attach:
            res.attach(MIMEText(attach, 'html', 'UTF-8'))
        return res

    def send(self, mail):
        try:
            self.log.debug(
                'open smtp connection to "%s:%d"',
                self.args.mailserver, self.args.mailport
            )
            session = SMTP_SSL(self.args.mailserver, self.args.mailport)
            self.log.debug('smtp login as "%s"', self.args.mailuser)
            session.login(self.args.mailuser, self.args.mailpass)
            self.log.info(
                'sending mail from "%s" to "%s"',
                self.args.mailuser, self.args.mailto
            )
            session.sendmail(
                self.args.mailuser, self.args.mailto,
                mail.as_string().encode('UTF-8')
            )
            return True
        except SMTPException as ex:
            self.log.exception(ex)
        finally:
            session.quit()
        return False

    def notify(self, dts, tag, *, content, attach=False):
        def recurse(item):
            if isinstance(item, str):
                return item
            if isinstance(item, tuple):
                return recurse(choice(item))
            return ''.join(recurse(it) for it in item)

        post = self.query(dts)
        if not post:
            return True
        self.log.info('ok! got something for "%s"', post.title)
        subject = '''
            [{name}] {title} ({tag}) Es tagt das ehrenwerte Plenum
        '''.strip().format(name=self.args.botname, title=post.title, tag=tag)
        text = '\n'.join([
            recurse(self.__intro), '', recurse(content),
            recurse(self.__links), '', recurse(self.__outro), ''
        ]).format(botname=self.args.botname, link=post.link)

        mail = self.mail(subject, text, attach=post.body if attach else False)
        self.log.info(
            'ok! generated mail "%s"\n\n%s', subject,
            '\n'.join('>> {}'.format(tx) for tx in text.splitlines())
        )
        if self.args.dry:
            self.log.error('dry! sending no mail for "%s"', post.title)
            return True
        self.log.info('ok! sending mail for "%s"', post.title)
        return self.send(mail)

    def __call__(self):
        self.log.debug('-' * 32)
        today = datetime.today()
        return all([
            self.notify(
                today - timedelta(days=1), 'Gestern',
                content=self.__yesterday, attach=True
            ),
            self.notify(
                today, 'Heute',
                content=self.__today
            ),
            self.notify(
                today + timedelta(days=2), 'Übermorgen',
                content=self.__after_tomorrow
            ),
            self.notify(
                today + timedelta(days=7), 'Nächste Woche',
                content=self.__next_week
            ),
        ])

    __yesterday = ['Gestern ', (
        'tagte das ehrenwerte', 'tagte das', 'war', 'war ein',
        'war wieder ein', 'war mal wieder ein', 'gings gut ab auf dem',
        'hatten wir viel Spaß auf dem', 'war es recht flauschig auf dem',
        'traf man sich zum'
    ), ' Plenum! ', (
        'Im Anhang', 'Als Anhang', 'Als Attachement', 'Unten', 'Beigefügt',
        'Mit dabei', 'Dazu', 'In der nächsten Mail als Word-Attachment',
        'Angehängt', 'Angeheftet'
    ), ' ', (
        ['die ', (
            'aktuelle Fassung', 'momentane Fassung', 'Abschrift',
            'Niederschrift', 'Wikiseite', 'Kopie',
        )], ['das ', (
            'Protokoll', 'Resultat', 'Ergebnis', 'was sich dazu so findet',
            'magische Protokoll', 'feine Protokoll'
        )]
    ), ('.', '.', '.', '!', '!!')]

    __today = [(
        'Freut euch', 'Jauchzet', 'Frohlocket', 'Super Toll', 'Mega Dufte',
        'Hammer hart', 'Mega', 'Erfolg',
        [('Totally', 'Depressingly'), ' Awesome'],
        [('Große', 'Riesen'), ' Freude'],
    ), ('.', '!', '!', '!!'), ' ', (
        'Heute', 'Heute Abend', 'Noch heute', 'An diesem Abend',
    ), ' ist Plenum', ('.', '!', '!!', '!!', '!!!'), ' Bitte ', (
        'kurz halten', 'kurz fassen', 'nicht abschweifen',
        'mitschreiben (Danke! <3)', 'beim Punkt bleiben',
    ), ' und ', (
        'zahlreich erscheinen', 'gute Laune mitbringen',
        'das Handtuch nicht vergessen', 'freundlich bleiben',
        'sachlich bleiben', 'brav bleiben', 'nichts vergessen',
    ), ('.', '.', '.', '!', '!!')]

    __after_tomorrow = [
        ('Übermorgen', ['In ', ('zwei', '2'), ' Tagen']), ' ist ', (
            'es wieder so weit', 'mal wieder Plenum', 'das nächste Plenum',
            'ein Plenum auf der Tagesordnung', 'ein Plenum fällig',
            'Plenum', 'Plenumstag', 'eine Tagung des ehrenwerten Plenums',
        ), ('.', '.', '.', '!', '!!'), ' Jezt ist die Zeit um ', (
            'Themenvorschläge einzutragen', 'Wünsche zu äußern',
            'das Wiki zu füllen', 'Themen einzutragen', 'zu partizipieren',
            'sich einzubringen', 'Stichpunkte zu sammeln',
            'sich das alles mal anzuschauen'
        ), ('.', '.', '..', '!', '!', '!!')
    ]

    __next_week = [
        'In ', ('genau', 'exakt', 'präzise'), ' ',
        ('einer Woche', 'sieben Tagen'), ' ist ', (
            'es wieder so weit', 'mal wieder Plenum', 'das nächste Plenum',
            'ein Plenum auf der Tagesordnung', 'ein Plenum fällig',
            'Plenum', 'Plenumstag', 'eine Tagung des ehrenwerten Plenums',
        ), ('.', '.', '.', '!', '!!'), ' Bis dahin bitte ', (
            'das Protokoll füllen', 'Themenvorschläge liefern',
            'Dinge ins Wiki eintragen', 'Stichpunkte sammeln',
            'fleißig Themen eintragen', 'Sachen ins Protokoll eintragen',
            [('cool', 'entspannt', 'relaxed'), ' bleiben'],
            ['einfach ', ('so weitermachen', 'alles ignorieren')],
        ), ('.', '.', '..', '!', '!', '!!')
    ]

    __links = [([
        ('Dieser', 'Der'), ' Link ', (
            ['ist ', (
                'der Weg', 'der Schlüssel', 'der Link', 'das Einganstor',
                'der direkte Draht', 'die Verbindung'
            ), ' zum'], [(
                'ist', 'beinhaltet', 'referenziert', 'verweist auf',
                'linkt auf', 'führt auf', 'zeigt auf',
            ), ' das'],
        ), ' Protokoll'
    ], [
        ('Das ist der', 'Hier der', 'Der'), ' Link ', (
            ['auf das ', (
                'aktuelle', 'momentane', 'schöne', 'tolle', 'feine'
            )], ['zum ', (
                'aktuellen', 'neuesten', 'feinsten vom', 'tollen', 'feinen'
            )], ['zur ', (
                'aktuellen Fassung', 'Wikiseite', 'Seite', 'Online-Ausgabe'
            ), ' vom'],
        ), ' Protokoll'
    ], [
        'Das Protokoll ', (
            ['ist ', (
                'hinter', 'eingelagert in', 'online mit', 'anwählbar mit',
                'abrufbar mit', 'lesbar mit', 'nun protokolliert mit',
            )], [(
                'findet man', 'erreicht man', 'in neuester Fassung',
                'im Wiki', 'immer frisch', '"Sonder-Edition"'
            ), ' mit'],
        ), ' diesem Link'
    ]), ('.', ':', '!'), '\n\n{link}']

    __intro = [([
        ('Schönen', 'Guten', 'Einen wunderbaren', 'Schönen guten'), ' ',
        ('Tag', 'Abend'), ('', '', '', ' an alle', ' euch allen'),
    ], [
        ('Liebe', 'Werte', 'Geehrte', 'Sehr geehrte'), ' ', (
            'Anwesende', 'Gemeinde', 'Menschen', 'Wesen', 'Lebensformen',
            'Chaoten', 'Freunde der Volksmusik', 'Genossen', 'Nutzer', 'Leser',
            ['Damen und Herren', ('', ', liebe Kinder')],
            ['Teilnehmer', ('', '', '', ' der Umfrage')],
        )
    ]), ('.', '.', '.', '..', ',', ',', '!', '!!', '!!1')]

    __outro = [(
        'Bis bald', 'Gehabt euch wohl', 'Man sieht sich',
        'Es grüßt', 'Mit freundlichen Grüßen', 'Freudlichst',
        ['Auf ', ('Wiedersehen', 'Wiederhören', 'bald')],
        ['Einen ', (
            'schönen', 'guten', 'wunderbaren', 'tollen'
        ), ' ', ('Tag', 'Abend'), ' ', ('noch', 'wünscht')],
        [('HDL', 'HDGDL'), ' <3', ('', [' ', ('lol', 'omg', 'xD')])],
        [('Liebe', 'Schöne'), ' Grüße'],
    ), ('', '', '', [', ', (
        'bekannt durch Film Funk und Fernsehen', 'Ehrenmann',
        '(hat übrigens Werbeplätze zu vermieten)', 'immer freundlich',
        'mag es flauschig', 'the one and only', 'Schiff Ahoi',
        ['es freut sich', ('', ' wie Schnitzel')],
    )]), ',\n    {botname}']


if __name__ == '__main__':
    exit(not Plenarsaal()())
